//
// Created by Shvana on 17/06/2020.
//
// Sum of n Numbers in a Loop using while

#include "stdio.h"

int N;
int i;
int sum;
int number;

int main() {
    printf("Enter how many number to add: ");
    scanf_s("%d", &N);

    i = 0;
    while (i<N){
        printf("Enter number %d:", i + 1);
        scanf_s("%d", &number);
        sum = sum + number;
        i++;
    }
    printf("Sum: %d", sum);
}