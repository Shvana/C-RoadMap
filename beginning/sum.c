//
// Created by Shvana on 17/06/2020.
//
// Sum of Three Numbers

#include "stdio.h"

int a,b,c,sum;

int main (){
    printf("Enter number a: ");
    scanf_s("%d", &a);

    printf("Enter number b: ");
    scanf_s("%d", &b);

    printf("Enter number C: ");
    scanf_s("%d", &c);

    sum = a + b + c;
    printf("Sum: %d", sum);
}